### What is this repository for? ###

This is a voice controlled chess game made using WPF and C#.
Version 1.0


### How do I get set up? ###
Visual Studio 2017 is required. This is due to the fact that in the initial development of the game, certain code structures where written using
 C# 7.0 for a test. The dot net version is 4.6.2


### Who do I talk to? ###

For any information email me at avokeselorm@live.com